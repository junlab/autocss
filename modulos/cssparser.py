# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 11 17:27:31 2017

@author: Valdir Dias Silva
Based http://www.thelinuxdaily.com/2011/05/python-script-to-grab-all-css-for-given-urls/
"""

import urllib.request
from urllib.request import urlopen
from urllib.parse import urlparse
from bs4 import BeautifulSoup 
from html.parser import HTMLParser


class ParserPage(HTMLParser):
    
    def __init__(self):
        HTMLParser.__init__(self)
        self.doc = {}
        self.path = self.doc
        self.p = False
        self.css = {}
        self.css_info = {}
        self.urls = {
            'bootstrap': "https://v4-alpha.getbootstrap.com/examples/jumbotron/",
            'derekhildreth': "http://derekhildreth.com",
            'thelinuxdaily': "http://thelinuxdaily.com",
            'myurl1': "http://myurl1.com"}
        self.headers = {'User-Agent':' Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0)'}
    
    def fetch_css(self, url):
        try:
            req = urllib.request.Request(url, data = None, headers = self.headers)
            response = urlopen(req)
            html_data =  response.read()
            response.close()
            
            soup = BeautifulSoup(html_data, 'html.parser')
            ext_styles = soup.findAll('link', rel="stylesheet")
            int_styles = soup.findAll('style', type="text/css")
         
            int_css_data = ''
            int_found = 1
            ic = 0
            if len(int_styles) != 0:
                for i in int_styles:
                    ic += 1
                    int_css_data += i.find(text=True)
            else:
                int_found = 0
        
            ext_css_data = ''
            ext_found = 1
            ec = 0
            if len(ext_styles) != 0:
                for i in ext_styles:
                    if self.is_absolute(i['href']) == False:
                        css_url = url + '/' + i['href'] 
                        self.p("Found external stylesheet: " + url)
                        ec += 1
                    else:
                        if i['href'][0:2] == "//":
                            css_url = "http:"+i['href']
                            ec += 1
                        else:
                            css_url = i['href']
                            ec += 1
                    self.p("Found external stylesheet: " + css_url)
                    
                req = urllib.request.Request(css_url, data=None, headers = self.headers)
                response = urlopen(req)
                ext_css_data += str(response.read())
                response.close()
            else:
                ext_found = 0
                #p("No external stylesheets found")
                print ("Internal stylesheet:  " + str(ic) +" - external stylesheet: "+ str(ec))
                all_css_data = int_css_data + ext_css_data
            
            return all_css_data, int_found, ext_found, ec, ic
        
        except Exception as e:
            print("Error: "+str(e))
            return "",0,0,0,0
        
          
    def get_css(self, url):
        self.p("nFetching: " + url)
        self.p("--------------------------------------------------------------------------------")
        out, int_found, ext_found, ec, ic = self.fetch_css(url)
        if ext_found == 1 or int_found == 1:
            self.css_info.update({"url":url, "external":ec, "internal":ic, "state":"successfully", "css": out})
            self.p("Styles successfully!")
            self.p("\n\n")
            
            return out
        
        elif out == "":
            self.css_state.update({"url":url, "external":ec, "internal":ic, "state":"erro", "css": ""})
            self.p("Error: URL not found!")
            self.p("\n\n")
        else:
            self.p("No styles found for " + url + "n")
            self.css_state.update({"url":url, "external":ec, "internal":ic, "state":"no style", "css": ""})
        
        return ""
                
                
    def p(self, log):
        if self.p == True:
            print(log)
            
            
    def is_absolute(url):     
        return bool(urlparse(url).netloc)
        
        
    def handle_starttag(self, tag, attrs):
        if tag in self.path:
            self.path[tag].update({len(self.path[tag]):{"style":{},"position":[]}})
        else:
            self.path.update({tag:{}})
            print(tag)
            self.path[tag].update({len(self.path[tag]):""})
            
            
    def handle_endtag(self, tag):
        self.oc = True
        
    
    def handle_data(self, data):
        print(data)
    
    
    @property
    def json(self):
        return self.doc
         
    @staticmethod
    def parser(self, url, raise_exception = True):
        page = ParserPage()
        
        self.url = url
        
        for k, v in self.urls.items():
            self.css = self.get_css(v)
            page.feed(v)
        
        return parser.json
    
            
      
