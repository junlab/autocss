from html.parser import HTMLParser


raw = '''
	<body>
		<div class="wrapper">
			<header>
				<h1>Yoko's Kitchen</h1>
			</header>
			<section class="courses">
				<article>
					<figure>
						<img src="images/bok-choi.jpg" alt="Bok Choi" />
						<figcaption>Bok Choi</figcaption>
					</figure>
				</article>    
				<article>
					<figure>
						<img src="images/teriyaki.jpg" alt="Teriyaki sauce" />
						<figcaption>Teriyaki Sauce</figcaption>
					</figure>
					<hgroup>
						<h2>Sauces Masterclass</h2>
						<h3>One day workshop</h3>
					</hgroup>
				</article>    
			</section>
			<aside>
				<section class="popular-recipes">
					<h2>Popular Recipes</h2>
					<a href="">Yakitori (grilled chicken)</a>
				</section>
				<section class="contact-details">
					<h2>Contact</h2>
					<p>Yoko's Kitchen<br />
						27 Redchurch Street<br />
						Shoreditch<br />
						London E2 7DP</p>
				</section>
			</aside>
			<footer>
				&copy; 2011 Yoko's Kitchen
			</footer>
		</div>
      <div><div>u</div></div>
	</body>
''' 


# create a subclass and override the handler methods
class MyHTMLParser(HTMLParser):
    
    def __init__(self, raise_exception = True):
        HTMLParser.__init__(self)
        self.doc = {}
        self.path = self.doc
        self.oc = False
        self.pos = [] 
        
    @property
    def json(self):
        return self.doc
         
    @staticmethod
    def to_json(content, raise_exception = True):
        parser = MyHTMLParser(raise_exception = raise_exception)
        parser.feed(content)
        return parser.json
        
    def handle_starttag(self, tag, attrs):
        
        if tag in self.path:
            self.path[tag].update({len(self.path[tag]):""})
        else:
            self.path.update({tag:{}})
            print(tag)
            self.path[tag].update({len(self.path[tag]):""})
            
        
    def handle_endtag(self, tag):
        self.oc = True

    
    def handle_data(self, data):
        print(data)
        
# instantiate the parser and fed it some HTML
js = MyHTMLParser.to_json(raw)